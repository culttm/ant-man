$(function(){

    $('body').scrollspy({ target: '.scrollspy' })

    $(window).scroll(function() {

        if ($(".header").offset().top > $(window).height() / 2) {

            $(".header").addClass("second");

        } else {

            $(".header").removeClass("second");
        }
    });

    $('[href*="#"]').on('click', function(e) {
        var $anchor = $(this);
        $('html, body').stop(true, true).animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 700);

        document.location.hash = $anchor.attr('href');

        return false;
    });


    var setHeightTo = function(el, h, w){
      if(w > 640){
          $(el).each(function() {
              var that = $(this);
                  that.css('min-height', h);
          });        
      }else{
          $(el).each(function() {
              var that = $(this);
                  that.css('min-height', '');
          });
      }

    };

    setHeightTo('.js-full-height', $(window).height(),  $(window).width());

    $(window).on('resize', function(event) {
        setHeightTo('.js-full-height', $(window).height(),  $(window).width());
    });





    var sync1 = $('.big-photos'),
        sync2 = $('.thumbs');


      sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: true,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
      });
     
      sync2.owlCarousel({
        items : 5,
        itemsDesktop      : [1199,5],
        itemsDesktopSmall     : [979,4],
        itemsTablet       : [768,3],
        itemsMobile       : [479,2],
        pagination:false,
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
      });
     
      function syncPosition(el){
        var current = this.currentItem;
        sync2
          .find(".owl-item")
          .removeClass("synced")
          .eq(current)
          .addClass("synced")
        if(sync2.data("owlCarousel") !== undefined){
          center(current)
        }
      }
     
      sync2.on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
      });
     
      function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
          if(num === sync2visible[i]){
            var found = true;
          }
        }
     
        if(found===false){
          if(num>sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", num - sync2visible.length+2)
          }else{
            if(num - 1 === -1){
              num = 0;
            }
            sync2.trigger("owl.goTo", num);
          }
        } else if(num === sync2visible[sync2visible.length-1]){
          sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
          sync2.trigger("owl.goTo", num-1)
        }
        
      }





//========================================= ABOUT =============================================
//

      function build(){

         

          var items = $(this.$owlItems),
              arr = [];

          $.each(items, function(index, val) {
              var elText = $(val).find('.item').data('dot-text');
              var anchorText = $(val).find('.item').data('anchor-text');
              
              arr.push(elText);

              $('.custom-pagination').append('<div data-to-slide="'+index+'" data-anchor="'+anchorText+'" class="di"><span class="n">0'+(index+1)+'</span><span class="n2">'+elText+'</span></div>')
          });

          $('.di:contains('+arr[this.owl.currentItem]+')').addClass('active').siblings().removeClass('active');



        setTimeout(function(){

          var ancArray = [],
              hash = document.location.hash;



          $('[data-anchor]').each(function(index, el) {
               ancArray.push('about-'+ $(this).data().anchor)         
          });

          console.log(ancArray)

          if(isInArray(hash.substring(1), ancArray)){

            var owl = $("#dsc-rotator").data('owlCarousel');

            owl.jumpTo($('[data-anchor="'+hash.substring(7)+'"]').index());

            $('html, body').stop(true, true).animate({
                scrollTop: $('#about').offset().top
            }, 700);

          }


        }, 1500);


        $('.di').on('click', function(event) {

          document.location.hash =  'about-' + $(this).data().anchor
          
           var owl = $("#dsc-rotator").data('owlCarousel');

          var t = $(this).data('to-slide')

            owl.goTo(t);
            
            return false;
        });





      };



      function isInArray(value, array) {
        return array.indexOf(value) > -1;
      }


      $('#dsc-rotator').owlCarousel({
        items: 1,
        itemsDesktop      : [1199,1],
        itemsDesktopSmall : [979,1],
        itemsTablet       : [768,1],
        itemsMobile       : [479,1],
        nav: true,
        autoHeight : true,
        afterInit: build,
        afterAction: function(){
            $('.di').eq(this.owl.currentItem ).addClass('active').siblings().removeClass('active')
        }
      });


        var owl = $("#dsc-rotator").data('owlCarousel');

        $('.custom-arr.prev').on('click', function(){
            owl.prev()
        });

        $('.custom-arr.next').on('click', function(){
            owl.next()
        });





// ===============================================================================





     

      $('.soc').on('click', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        window.open(url, '', 'toolbar=0,status=0,width=554,height=436, top=100, left=200');
      });


      $('[data-switch-target]').on('click', function(event) {
        event.preventDefault();
        var target = $(this).data('switch-target');
        $(this).addClass('active').siblings('[data-switch-target]').removeClass('active');
        $('#'+target).addClass('active').siblings('.tab-panel').removeClass('active');
        if(target == 'tab-2'){
          $('.switch').addClass('un');
        }else{
          $('.switch').removeClass('un');
        }

        if($(window).width() <= 768 ){
          owl.jumpTo(2);
          owl.jumpTo(3);
        }
      });
    
});